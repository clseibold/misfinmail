package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/clseibold/misfin-server/gembox"
	"gitlab.com/clseibold/misfin-server/gemmail"
	"gitlab.com/clseibold/misfin-server/misfin_client"
)

var MailCommand = &cobra.Command{
	DisableFlagsInUseLine: true,
	Use:                   "misfinmail",
	Short:                 "Misfin mail command. This version uses Misfin(C) spec.",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var GemboxReadList = false

// TODO: reply, reply_all, and forward
// TODO: delete marks for deletion. Use retrieve to remove this mark on a message.
// To and BCC (Blind Carbon Copy) - send mail to several people
// Fix input stuff, since it got messed up when I turned off CGO

func init() {
	var SendCommand = &cobra.Command{
		Use:   "send address [message]",
		Short: "Sends gemmail to misfin address using sender cert. If no message arg provided, it prompts you for the subject and message body.",
		Run:   SendCmd,
	}
	SendCommand.Flags().StringVar(&FlagsConfiguration.CertFilename, "cert", "", "Sender certificate path. By default, will use certificate path from config file.")

	var ReadCommand = &cobra.Command{
		Use:   "read [num]",
		Short: "Reads gemmails from your gembox. If a number is specified, goes directly to that message.",
		Run:   ReadCmd,
	}
	ReadCommand.Flags().StringVar(&FlagsConfiguration.GemboxFilename, "gembox", "", "Gembox path. By default, will use gembox path from config file.")
	ReadCommand.Flags().BoolVar(&GemboxReadList, "list", false, "List a summary of messages from gembox.")

	var FetchCommand = &cobra.Command{
		Use:   "fetch",
		Short: "Downloads your gembox from the configured gemini fetch address to your configured local gembox (or the path specified by the gembox flag).",
		Long:  "Downloads your gembox from the configured gemini fetch address to your configured local gembox (or the path specified by the gembox flag) \nusing your sender cert as the client cert. Errors out if no fetch address is configured.",
		Run: func(cmd *cobra.Command, args []string) {
			config := getConfigValuesForCommand()
			if config.GeminiFetchAddress == "" {
				fmt.Printf("Gemini Fetch Address not configured.\n") // TODO: Specify the command to use to configure it.
			} else {
				fmt.Printf("Fetching from '%s' to gembox '%s'.\n", config.GeminiFetchAddress, config.GemboxFilename)
				fetchGembox(config)
			}
		},
	}
	FetchCommand.Flags().StringVar(&FlagsConfiguration.GemboxFilename, "gembox", "", "Gembox path to download to. By default, will use gembox path from config file.")
	FetchCommand.Flags().StringVar(&FlagsConfiguration.GeminiFetchAddress, "address", "", "Specify the gemini fetch address. By default, will use the address from the config file.")

	var ResetGemboxCommand = &cobra.Command{
		Use:   "reset-gembox",
		Short: "Resets your local gembox, clearing it, and erases your last fetch datetime. This allows you to re-fetch all of your mails from the fetch address.",
		Long:  "Resets your local gembox, clearing it, and erases your last fetch datetime. This allows you to re-fetch all of your mails from the fetch address.",
		Run: func(cmd *cobra.Command, args []string) {
			config := getConfigValuesForCommand()
			config.LastFetchedGemmailDatetime = time.Time{}
			saveConfig(config)

			os.WriteFile(config.GemboxFilename, []byte(""), 0755)
		},
	}
	ResetGemboxCommand.Flags().StringVar(&FlagsConfiguration.GemboxFilename, "gembox", "", "Gembox path to download to. By default, will use gembox path from config file.")

	var ConfigureCommand = &cobra.Command{
		Use:   "config field value",
		Short: "Changes your configuration file.",
		Run:   ConfigureCmd,
	}

	var VersionCommand = &cobra.Command{
		Use:   "version",
		Short: "Display version info.",
		Run:   VersionCmd,
	}

	MailCommand.AddCommand(SendCommand, ReadCommand, FetchCommand, ResetGemboxCommand, ConfigureCommand, VersionCommand)
}

func SendCmd(cmd *cobra.Command, args []string) {
	config := getConfigValuesForCommand()
	var message string

	if len(args) <= 0 {
		fmt.Printf("Error: No misfin address specified.\n")
		os.Exit(1)
	} else if len(args) <= 1 {
		var builder strings.Builder

		reader := bufio.NewReader(os.Stdin)
		for {
			fmt.Printf("Subject: ")
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				os.Exit(1)
			} else if readErr != nil {
				fmt.Printf("Could not read input.\n")
				continue
			}
			builder.WriteString("# " + strings.ReplaceAll(strings.TrimSpace(input), "\r\n", "\n") + "\n")
			break
		}

		fmt.Printf("Type your message, ending with '.' on line by itself: \n")
		for {
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				os.Exit(1)
			} else if readErr != nil {
				panic("Failed to read input.\n")
			}

			if strings.HasPrefix(input, ".") && len(strings.TrimSpace(input)) == 1 {
				// '.' on a line by itself means you are done with the message, so break
				break
			} else {
				builder.WriteString(strings.ReplaceAll(input, "\r\n", "\n"))
			}
		}
		message = builder.String()
	} else { // TODO: Handle message with no address
		message = strings.Join(args[1:], " ")
	}

	address := strings.TrimSpace(args[0])
	//url.Parse(address) // TODO
	c := misfin_client.DefaultClient

	if config.CertFilename != "" {
		var readErr error
		config.CertFile, readErr = os.ReadFile(config.CertFilename) // TODO: This command is probably running multiple times now. Check this
		if readErr != nil {
			fmt.Printf("Couldn't open certificate at location '%s'. Make sure the cert is configured correctly in '%s' and that the cert is accessible by the user.\n%s\n", config.CertFilename, "~/misfinmail.conf", readErr)
			return
		}
	}
	var redirectAttempt = 0
	var misfinB = false
	for {
		gmail := gemmail.CreateGemMailFromBody(message)
		msgToSend := ""
		if misfinB {
			msgToSend = gmail.String_MisfinB()
		} else {
			msgToSend = gmail.String()
		}
		resp, err := c.SendWithCert("misfin://"+address, config.CertFile, config.CertFile, msgToSend) // TODO: Make sure this strips all '\r'
		if err != nil {
			fmt.Printf("Sending gemmail failed: %s\n", err)
		} else if (resp.Status == 59 || resp.Status == 40 || resp.Status == 60) && !misfinB { // Invalid request, try with Misfin(B) format
			misfinB = true
			c = misfin_client.DefaultClient_MisfinB
			redirectAttempt = 0
			continue
		} else if redirectAttempt < 5 && (resp.Status == misfin_client.StatusRedirect || resp.Status == misfin_client.StatusRedirectPermanent) {
			address = strings.TrimPrefix(strings.TrimSpace(resp.Meta), "misfin://")
			redirectAttempt += 1
			continue
		} else {
			fmt.Printf("%d %s\n", resp.Status, resp.Meta)
		}
		break
	}
}

func ReadCmd(cmd *cobra.Command, args []string) {
	config := getConfigValuesForCommand()

	reader := bufio.NewReader(os.Stdin)
	gemboxContents, readErr := os.ReadFile(config.GemboxFilename)
	if errors.Is(readErr, os.ErrNotExist) /*|| errors.Is(readErr, fs.ErrNotExist)*/ {
		// Ignore
	} else if readErr != nil {
		panic(readErr)
	}
	var gbox gembox.GemBox
	if config.Version == "0.4.6c" { // 0.4.6c was the first version to use old misfin(C) gembox format
		gbox = gembox.ParseGemBox_MisfinC_old(config.GemboxFilename, string(gemboxContents))
	} else if strings.HasSuffix(config.Version, "c") { // 0.4.7c was the first version to use new gembox format
		gbox = gembox.ParseGemBox(config.GemboxFilename, string(gemboxContents))
	} else { // All non-"c" versions used old misfin(B) gembox format
		gbox = gembox.ParseGemBox_MisfinB_old(config.GemboxFilename, string(gemboxContents))
	}

	num := len(gbox.Mails)
	if num == 1 { // Singular
		fmt.Printf("You have %d gemmail in gembox '%s'.\n", num, config.GemboxFilename)
	} else { // Plural
		fmt.Printf("You have %d gemmails in gembox '%s'.\n", num, config.GemboxFilename)
	}

	// If -list flag was set, print out a summary of messages
	if GemboxReadList {
		// TODO: If num argument was specified, what should this do?
		for i, m := range gbox.Mails {
			printMessage(i+1, m, true, false)
			// Prompt for input.
		}
	} else {
		messagesToDelete := make([]bool, len(gbox.Mails))
		for i := 0; i < len(gbox.Mails); i++ {
			messagesToDelete[i] = false
		}
		abort := false
		i := 0

		// Check if number was specified
		if len(args) == 1 {
			// Parse the number and set it to i.
			numStr := strings.TrimSpace(strings.ToUpper(args[0]))
			num, atoiErr := strconv.Atoi(numStr)
			if atoiErr != nil {
				fmt.Printf("Warning: passed in an invalid number.\n")
			} else if num < 1 || num > len(gbox.Mails) {
				fmt.Printf("Warning: number not in valid range of 1 to %d\n", len(gbox.Mails))
			} else {
				i = num - 1
			}
		}

	outer:
		for i < len(gbox.Mails) {
			m := gbox.Mails[i]
			fmt.Printf("\n")
			printMessage(i+1, m, false, messagesToDelete[i])
			fmt.Printf("\n")

			// Prompt for input.
			for {
				fmt.Printf("|> Delete #%d locally? ('help' for more commands) ", i+1)

				cmd, readErr := reader.ReadString('\n')
				if readErr == io.EOF { // Ctrl+C pressed. Abort
					abort = true
					os.Exit(1)
					//break outer
					return
				} else if readErr != nil {
					fmt.Printf("Could not understand input.\n")
					continue
				}
				cmd = strings.TrimSpace(strings.ToUpper(cmd))

				if cmd == "YES" || cmd == "Y" {
					messagesToDelete[i] = true
					break
				} else if cmd == "NO" || cmd == "N" {
					messagesToDelete[i] = false
					break
				} else if cmd == "REPRINT" {
					fmt.Printf("\n")
					printMessage(i+1, m, false, messagesToDelete[i])
					fmt.Printf("\n")
					continue // Continue prompting until YES, NO, QUIT, or ABORT have been typed in
				} else if cmd == "LIST" {
					fmt.Printf("\n")
					for i, m := range gbox.Mails { // NOTE: i is being shadowed here
						printMessage(i+1, m, true, messagesToDelete[i])
					}
					fmt.Printf("\n")
				} else if strings.HasPrefix(cmd, "GOTO") {
					parts := strings.SplitN(cmd, " ", 2)
					if len(parts) == 2 {
						// Check if cmd has second argument
						numStr := strings.TrimSpace(strings.ToUpper(parts[1]))
						num, atoiErr := strconv.Atoi(numStr)
						if atoiErr != nil {
							fmt.Printf("Error: not a valid number.\n")
							continue
						} else if num < 1 || num > len(gbox.Mails) {
							fmt.Printf("Error: not in valid range of 1 to %d\n", len(gbox.Mails))
							continue
						} else {
							i = num - 2 // Go to the message before, because i will increment after this break
							break
						}
					} else {
						// If no second argument, then prompt for the message #
						cancel := false
						for {
							fmt.Printf("Goto which Message #? ('x' to cancel): ")
							numStr, readErr := reader.ReadString('\n')
							if readErr == io.EOF { // Ctrl+C pressed. Abort
								abort = true
								break outer
							} else if readErr != nil {
								fmt.Printf("Could not understand input.\n")
								continue
							}
							numStr = strings.TrimSpace(strings.ToUpper(numStr))
							if numStr == "X" {
								break
							}

							// Verify input is a number
							num, atoiErr := strconv.Atoi(numStr)
							if atoiErr != nil {
								fmt.Printf("Error: not a valid number.\n")
								continue
							} else if num < 1 || num > len(gbox.Mails) {
								fmt.Printf("Error: not in valid range of 1 to %d\n", len(gbox.Mails))
								continue
							} else {
								i = num - 2 // Go to the message before, because i will increment after this break
								break
							}
						}
						if cancel {
							continue
						} else {
							break
						}
					}
				} else if cmd == "QUIT" || cmd == "EXIT" || cmd == "Q" || cmd == "E" {
					// Don't add to messagesToDelete, just quit
					break outer
				} else if cmd == "ABORT" {
					abort = true
					fmt.Printf("Abording without updating gembox...\n")
					break outer
				} else {
					if cmd == "HELP" {
						fmt.Printf("\nList of commands:\n")
					} else {
						fmt.Printf("\nUnknown Command. Printing Help:\n")
					}
					fmt.Printf("yes - deletes the gemmail locally\n")
					fmt.Printf("no - retains the gemmail locally\n")
					fmt.Printf("reprint - reprints the gemmail\n")
					fmt.Printf("list - lists summary of all messages\n")
					fmt.Printf("goto - prompts you for the message to move to\n")
					fmt.Printf("quit - retains the gemmail locally and quits the program\n")
					fmt.Printf("abort - retains all gemmails locally, even ones previously marked for deletion, and quits the program\n")
					fmt.Printf("help - lists commands\n\n")
					continue
				}
			}
			i += 1 // Always increment
		}

		// Delete the messages
		if !abort {
			offset := 0
			for i, v := range messagesToDelete {
				if v {
					gbox.RemoveGemMail(i - offset)
					offset += 1
				}
			}
		}

		// Save changes to gembox
		gemboxData := gbox.String()
		writeErr := os.WriteFile(config.GemboxFilename, []byte(gemboxData), 0600)
		if writeErr != nil {
			panic(writeErr)
		}
	}

}

func printMessage(index int, gmail gemmail.GemMail, summary bool, markedForDeletion bool) {
	if gmail.Subject == "" {
		gmail.Subject = "[No Subject]"
	}
	if summary {
		subject := string(gmail.Subject)
		if markedForDeletion {
			subject = "*" + subject
		}
		if index == 0 {
			fmt.Printf("Message: %s (From %s on %s)\n", subject, gmail.Senders[0].Address, gmail.Timestamps[0].Value.Local().Format(time.DateOnly))
		} else {
			fmt.Printf("Message %d: %s (From %s on %s)\n", index, subject, gmail.Senders[0].Address, gmail.Timestamps[0].Value.Local().Format(time.DateOnly))
		}
		return
	} else {
		if index == 0 {
			fmt.Printf("Message: %s\n", string(gmail.Subject))
		} else {
			fmt.Printf("Message %d: %s\n", index, string(gmail.Subject))
		}
	}

	lastSender := gmail.Senders[0]
	originalSender := lastSender
	if len(gmail.Senders) >= 2 {
		originalSender = gmail.Senders[len(gmail.Senders)-1]

		fmt.Printf("Last Sender (Forwarder): %s (%s)\n", lastSender.Address, lastSender.Blurb)
		fmt.Printf("Original Sender: %s (%s)\n", originalSender.Address, originalSender.Blurb)
	} else {
		fmt.Printf("Sender: %s (%s)\n", lastSender.Address, lastSender.Blurb)
	}

	if len(gmail.Timestamps) > 0 {
		fmt.Printf("Received on %s\n", gmail.Timestamps[0].Value.Local().Format(time.DateTime))
	}

	if len(gmail.Receivers) > 0 {
		fmt.Printf("Recipients: ")
		for r := range gmail.Receivers {
			fmt.Printf("%s ", r)
		}
		fmt.Printf("\n")
	}

	if markedForDeletion {
		fmt.Printf("Marked for deletion.\n")
	}

	fmt.Printf("\n")
	for _, l := range gmail.GemtextLines {
		fmt.Printf("%s\n", l.String())
	}
}

// TODO: Add interactive mode?
func ConfigureCmd(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		fmt.Printf("Please specify the field to configure: sender-cert, gembox, or gembox-fetch.\n\nconfig: ")
		cmd.Help()
		return
	} else if len(args) < 2 {
		fmt.Printf("Please specify a value for this field.\n")
		return
	}

	field := args[0]
	value := strings.Join(args[1:], " ")

	if field == "sender-cert" {
		UserConfiguration.CertFilename = value
	} else if field == "gembox" {
		UserConfiguration.GemboxFilename = value
	} else if field == "gembox-fetch" {
		// Make sure fetch address is valid
		URL, parseErr := url.Parse(value)
		if parseErr != nil {
			panic(parseErr)
		}
		if URL.Scheme == "" {
			URL.Scheme = "gemini"
		}
		if URL.Scheme != "gemini" {
			fmt.Printf("Please enter a gemini address.\n")
			return
		}
		UserConfiguration.GeminiFetchAddress = URL.String()
	}

	UserConfiguration.Version = Version

	saveConfig(UserConfiguration)
}

func VersionCmd(cmd *cobra.Command, args []string) {
	fmt.Printf("Misfinmail version %s, Misfin(C) Version\n", Version)
}
