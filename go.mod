module gitlab.com/clseibold/misfinmail/v2

go 1.21.0

require (
	github.com/clseibold/go-gemini v0.0.0-20240226215632-c39755b92b21
	github.com/spf13/cobra v1.7.0
	gitlab.com/clseibold/misfin-server v0.0.0-20240501052059-82927bb116e7
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
