# Misfinmail

A misfin mail terminal client written in golang and modelled after the Multics mail commands. It will let you send gemmails using your sender cert and read gemmails from a gembox.

**Note:** I have also created a misfin server program, misfin client library, and gemmail and gembox parsers in golang, which you can find on the [misfin-server repo](https://gitlab.com/clseibold/misfin-server).

A feature that I am excited about having added is the ability to add a gemini fetch address for your gembox, where the command will fetch your gembox from gemini using the address, with the gemini client cert set to your sender cert. The fetched gembox will then be downloaded to your configured gembox filepath.

For those who have gemini servers, you can setup a route that serves your gembox and verifies that the client cert's fingerprint (SHA256) is the same as your sender cert's fingerprint. This is also a useful feature for those who want to become misfin mail hosters. You just need to provide the user with this fetch address.

For server hosters who choose not to use the gembox format to store gemmails on their server, you can create a route that loads all of the gemmails on the server and constructs, on-the-fly, them into the gembox format. To do this, you simply need to concatenate the gemmails onto each other, separating them with a line of `<=====`. Should be fairly simple to program.

I am considering adding a new configuration option to this misfin client to use a folder that contains individual gemmail files instead, but this is not something currently in the program.

Copyright: Christian Lee Seibold<br>
License: BSD-3-Clause

## Building/Installing

You can get precompiled binaries for x64 Windows and Linux on the [Releases Page](https://gitlab.com/clseibold/misfinmail/-/releases).

```
go build .
```

If you have golang, you can instead install the program using the following command:
```
go install gitlab.com/clseibold/misfinmail/v2@latest
```

It will install to the bin directory in your GOPATH. Make sure `$GOPATH/bin` is put into the PATH environment variable to be able to run the command.

## Running

On first run of the program, it will create a `misfinmail.conf` configuration file in your Home Directory (%USERPROFILE% on Windows). It will then prompt you for the path to your sender certificate and the path to your gembox. These will be placed into the config file and loaded on each run of the command.

It will also prompt if you want to set a gemini fetch address. This feature will fetch your gembox from the gemini address, using your sender cert as the gemini client cert, and save the gembox to your configured gembox path. This fetch will happen on every run of the command to ensure data is up to date.

Currently, the fetch command does not keep track of last message fetched, which means if you delete anything in your local gembox, the results will not be saved as the fetch will always overwrite your local gembox with the contents of the gembox from the fetch address. I'm currently considering ways of improving this situation.

If you run your own gemini server, you can setup a gemini fetch address by making a route to your gembox which requires a client certificate, and the client certificate SHA256 hash should match your sender cert's SHA256 hash (called the `fingerprint` in misfin docs).

### Help
```
misfinmail help
```

### Send Mail
Send mail with this command, providing the address. If you exclude the optional message argument, the command will prompt you for the subject and message body. The message body can be multiline. End the message body with a single `.` by itself on the last line.
```
misfinmail send address [message]
```

You can specify a certificate using the `--cert cert_path` flag. This will temporarily override the default certificate specified in the `~/misfinmail.conf` configuration file.

### Read from Gembox
This command will read from the gembox configured in your configuration file. You can provide the command an optional number argument to start at that message number.
```
misfinmail read [num]
```

You can specify a gembox using the `--gembox` flag. This will read from the specified gembox instead of the configured default gembox filepath.
```
misfinmail read --gembox archive.gembox
```

This command will print out each gemmail and prompt you for input on whether you want to delete that gemmail from your gembox or not. The commands are as follows:
* yes - deletes the gemmail
* no - retains the gemmail and moves you to the next gemmail
* reprint - reprints the current gemmail
* list - lists summary of all gemmails
* goto - prompts you for a message number to go to
* quit - retains the current gemmail and quits
* abort - retains all gemmails, even ones marked for deletion previously, and quits.
* help - prints this list of commands

You can also specify the `--list` command to list out a summary of the gemmail in your gembox.

### Fetch Command
This command will download a gembox from the configured gemini fetch address to the configured gembox filepath.
```
misfinmail fetch
```

You can pass the `--gembox` flag to specify a different gembox filepath from the configured default. The example below will fetch from the gemini fetch address to the archive.gembox file. The program *will not* change the gembox configured as the default in your configuration file.
```
misfinmail fetch --gembox archive.gembox
```

You can also pass in the `--address` flag to specify a gemini fetch address different from the configured default. The example below will fetch from the specified gemini address and store the gembox in the archive.gembox file.
```
misfinmail fetch --gembox archive.gembox --address gemini://auragem.letz.dev/mail/user/gembox
```

### Configuration

You can modify your configuration file anytime by using the `config` command.
```
misfinmail config field value
```

The current possible fields are:
* `gembox` - the path location of your gembox. Defaults to `~/.gembox` (or `%USERPROFILE%\.gembox` on Windows)
* `sender-cert` - the sender certificate to use to send mail. Should be the same as your mailbox server on your misfin mailserver.
* `gembox-fetch` - the gemini fetch address to fetch a gembox over gemini. Will make a gemini request using your sender cert as the gemini client cert.

The `gembox` and `sender-cert` fields take paths as their values. The `gembox-fetch` field takes a gemini address as its value.

## The Gemmail Format (From the Spec)

The gemmail format is like a gemtext file, with three new linetypes. The three new linetypes are as follows:
* **Sender Line** - a sender line starts with `<` followed by the sender misfin address. You can add a space and the blurb (aka. full name) after the misfin address, but this is optional. The general syntax is like so, with square brackets for optional characters `<[ ]mailbox@hostname.com[ First Last]`. When a message comes in to the receiving mailserver, you should append the incoming sender just below the last sender line of the gemmail.
* **Recipients Line** - a receivers line is a list of those who have received the gemmail. The linetype starts with a colon (`:`) and is followed by misfin addresses separated with a space. This line is useful if sending one gemmail to multiple people and you want those people to be able to know who else the gemmail was sent to as well as reply to all people involved. If a gemmail is sent to just one person, then there is no need for a recipients line. Note that the person you are directly sending a copy of the gemmail to does not need to be on the recipients line. When sending a copy to `two@example.com`, their address does not go on the recipients line, and then when you send to the next recipient `three@example.com`, include the address for `two@example.com`, but not for `three@example.com`.
* **Timestamp Line** - a timestamp line tells you when a message was received. The receiving mailserver adds a timestamp to the gemmail when the message is received. The line begin with `@` and is followed by an ISO-8601 format (and I believe always in UTC). Theoretically, this means each receiving mailserver will add their own timestamp, so if a gemmail is being forwarded, there will be multiple timestamps. The last timestamp should be the latest one, assuming mailservers always add their timestamp below the last timestamp in the gemmail so that the timestamps are in the order they are received.

Lastly, a message's subject is always assumed to be the first gemtext level-1 heading (the first line starting with `#`). All other lines should be interpreted as per the Gemini gemtext spec.

If you are forwarding a gemmail, keep all sender lines. Each receiving mailserver will add a sender line for each sender in order.

## Gembox (.gembox or .gmbox file) and Gembox parser

Gemboxes are gemmails with the divider `<=====\n` appearing on a separate line in-between each gemmail. This divider never appears at the beginning or end of a gembox file (they are always between two gemmails). When you receive mail, the mail will be appended to the gembox file and will be printed to the terminal.

The Gembox parser (in the `gembox` subdirectory) will simply split a multi-line string based on the divider linetype `<=====\n` to get each individual gemmail. Then, each gemmail will be passed to the Gemmail parser mentioned above. The GemBox struct contains a slice of GemMails.

Call the `String()` function on a GemBox variable to convert the gembox back into a string for saving. You can also append GemMails to the GemBox before converting it back into a string.

The divider was chosen because it starts with a gemmail linetype (the sender line, using the `<` character), however `=====` is an invalid address. This means readers that don't support gembox will either fail on the line or ignore it if they are trying to parse addresses correctly. The intention is that this hopefully introduces some backwards compatibility, however, there may be some other way of doing this better. I am not set on the specific divider that I have chosen, so feedback is welcome.

## Contact Me

You can contact me via misfin at the address `clseibold@auragem.letz.dev`.
