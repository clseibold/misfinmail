package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/clseibold/go-gemini"
	"gitlab.com/clseibold/misfin-server/gembox"
)

var Version string = "0.4.7c" // Appended with "c" for Misfin(C) version

type Config struct {
	GemboxFilename             string
	GeminiFetchAddress         string
	CertFilename               string
	CertFile                   []byte
	LastFetchedGemmailDatetime time.Time
	Version                    string // Version of client last used with config file.
}

var DefaultConfiguration = Config{"~/.gembox", "", "", []byte{}, time.Time{}, "0.4.6"}
var UserConfiguration = DefaultConfiguration
var FlagsConfiguration = Config{"", "", "", []byte{}, time.Time{}, "0.4.6"}

func main() {
	// Get home directory
	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	configFilename := filepath.Join(homeDir, "misfinmail.conf")
	file, readErr := os.ReadFile(configFilename)
	if /*errors.Is(readErr, fs.ErrNotExist) ||*/ errors.Is(readErr, os.ErrNotExist) || strings.TrimSpace(string(file)) == "" {
		reader := bufio.NewReader(os.Stdin)

		// If file doesn't exist. Create it.
		fmt.Printf("Config file doesn't exist. Creating as %q.\n", configFilename)
		createdFile, createErr := os.Create(configFilename)
		if createErr != nil {
			panic(createErr)
		}

		// Prompt for sender certificate filepath
		for {
			fmt.Printf("What is the path of your sender certificate? This is the cert you will use to send messages with: ")
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				// Abort program without saving file
				os.Exit(1)
			} else if readErr != nil {
				fmt.Printf("Could not read input.\n")
				continue
			}
			UserConfiguration.CertFilename = strings.TrimSpace(input)
			break
		}

		// Add sender certificate filepath to config
		fmt.Printf("Adding sender certificate to configuration file.\n")
		createdFile.WriteString("sender-cert: ")
		createdFile.WriteString(UserConfiguration.CertFilename)
		createdFile.WriteString("\n")

		// Prompt if you want to fetch gembox from gemini address
		YNResponse := ""
		for {
			fmt.Printf("Do you want to fetch your gembox from a gemini address? Y/N? ")
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				// Abort
				os.Exit(1)
			} else if readErr != nil {
				fmt.Printf("Could not read input.\n")
				continue
			}
			YNResponse = strings.ToUpper(strings.TrimSpace(input))
			if YNResponse != "YES" && YNResponse != "NO" && YNResponse != "Y" && YNResponse != "N" {
				fmt.Printf("Please repond with Y or N.\n")
				continue
			}
			break
		}
		fetching := false
		if YNResponse == "YES" || YNResponse == "Y" {
			fetching = true
			// Prompt for the gemini address
			for {
				fmt.Printf("What is the gemini address to fetch your gembox from? ")
				input, readErr := reader.ReadString('\n')
				if readErr == io.EOF {
					// Abort
					os.Exit(1)
				} else if readErr != nil {
					fmt.Printf("Could not read input.\n")
					continue
				}

				// Verify the input is a valid gemini address
				URL, parseErr := url.Parse(strings.TrimSpace(input))
				if parseErr != nil {
					fmt.Printf("Please enter a valid gemini address.\n")
					continue
				}
				if URL.Scheme == "" {
					URL.Scheme = "gemini"
				}
				if URL.Scheme != "gemini" {
					fmt.Printf("Please enter a valid gemini address.\n")
					continue
				}

				// Valid. Break from for loop
				UserConfiguration.GeminiFetchAddress = URL.String()
				break
			}

			// Add the fetch address to the config file
			createdFile.WriteString("gembox-fetch: ")
			createdFile.WriteString(UserConfiguration.GeminiFetchAddress)
			createdFile.WriteString("\n")

			createdFile.WriteString("version: ")
			createdFile.WriteString(Version)
			createdFile.WriteString("\n")

			fmt.Printf("Ok. Your gembox will be fetched from %q, using your sender cert (%q) as the gemini client cert.\n", UserConfiguration.GeminiFetchAddress, UserConfiguration.CertFilename)
		}

		// Prompt for gembox filepath
		if !fetching {
			fmt.Printf("What is the path to your gembox? [~/.gembox] ")
		} else {
			fmt.Printf("What is the path to your local gembox? Upon fetching your gembox from gemini, it will be downloaded to this file. [~/.gembox] ")
		}
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			os.Exit(1)
		} else if readErr != nil {
			input = ""
		}
		UserConfiguration.GemboxFilename = strings.TrimSpace(input)
		if UserConfiguration.GemboxFilename == "" {
			UserConfiguration.GemboxFilename = DefaultConfiguration.GemboxFilename
		}

		// Add gembox filepath to config
		fmt.Printf("Adding gembox path to configuration file.\n\n")
		createdFile.WriteString("gembox: ")
		createdFile.WriteString(UserConfiguration.GemboxFilename)
		createdFile.WriteString("\n")

		createdFile.Close()
		file, _ = os.ReadFile(configFilename) // TODO: Another Read error?
	} else if readErr != nil {
		panic(readErr)
	}

	// Parse configuration file
	parseConfig(string(file), &UserConfiguration)
	handleConfigValues(&UserConfiguration)
	handleGemboxConversion(&UserConfiguration)
	fetchGembox(UserConfiguration)
	MailCommand.Execute()
}

func parseConfig(data string, config *Config) {
	lines := strings.FieldsFunc(data, func(r rune) bool { return r == '\n' })

	for _, line := range lines {
		parts := strings.SplitN(line, ":", 2)
		field := strings.TrimSpace(parts[0])
		value := strings.TrimSpace(parts[1])

		if field == "sender-cert" && config.CertFilename == "" {
			config.CertFilename = value
		} else if field == "gembox" {
			config.GemboxFilename = value
		} else if field == "gembox-fetch" && config.GeminiFetchAddress == "" {
			config.GeminiFetchAddress = value
		} else if field == "last-fetch-datetime" {
			var err error
			config.LastFetchedGemmailDatetime, err = time.Parse(time.RFC3339, value)
			if err != nil {
				fmt.Printf("Couldn't read time in config file; %s\n", err)
			}
		} else if field == "version" {
			config.Version = value
		}
	}
}

// Assumes current configuration has already been parsed (tl;dr: call parseConfig, make changes to config values, then call this function to save the changes)
func saveConfig(config Config) {
	homeDir, _ := os.UserHomeDir()
	configFilename := filepath.Join(homeDir, "misfinmail.conf") // TODO

	var builder strings.Builder
	builder.WriteString("sender-cert: ")
	builder.WriteString(config.CertFilename)
	builder.WriteString("\n")

	if config.GeminiFetchAddress != "" {
		builder.WriteString("gembox-fetch: ")
		builder.WriteString(config.GeminiFetchAddress)
		builder.WriteString("\n")
	}

	builder.WriteString("gembox: ")
	builder.WriteString(config.GemboxFilename)
	builder.WriteString("\n")

	builder.WriteString("last-fetch-datetime: ")
	builder.WriteString(config.LastFetchedGemmailDatetime.Format(time.RFC3339))
	builder.WriteString("\n")

	// Always save with current version
	builder.WriteString("version: ")
	builder.WriteString(Version)
	builder.WriteString("\n")

	writeErr := os.WriteFile(configFilename, []byte(builder.String()), 0600)
	if writeErr != nil {
		panic(writeErr)
	}
}

func handleConfigValues(config *Config) {
	homeDir, _ := os.UserHomeDir()
	// Replace '~' with the homedir
	//if runtime.GOOS == "windows" {
	if strings.HasPrefix(config.CertFilename, "~") {
		config.CertFilename = filepath.Join(homeDir, strings.TrimPrefix(config.CertFilename, "~"))
	}
	if strings.HasPrefix(config.GemboxFilename, "~") {
		config.GemboxFilename = filepath.Join(homeDir, strings.TrimPrefix(config.GemboxFilename, "~"))
	}
	//}

	// Open sender cert
	config.CertFile, _ = os.ReadFile(config.CertFilename)
}

// Returns config values based on set flags or user's defaults. If a flag was not set, then that field uses the value from the user's defaults.
func getConfigValuesForCommand() Config {
	var result Config
	if FlagsConfiguration.CertFilename == "" {
		result.CertFilename = UserConfiguration.CertFilename
	} else {
		result.CertFilename = FlagsConfiguration.CertFilename
	}

	if FlagsConfiguration.GemboxFilename == "" {
		result.GemboxFilename = UserConfiguration.GemboxFilename
	} else {
		result.GemboxFilename = FlagsConfiguration.GemboxFilename
	}

	if FlagsConfiguration.GeminiFetchAddress == "" {
		result.GeminiFetchAddress = UserConfiguration.GeminiFetchAddress
	} else {
		result.GeminiFetchAddress = FlagsConfiguration.GeminiFetchAddress
	}

	result.LastFetchedGemmailDatetime = UserConfiguration.LastFetchedGemmailDatetime
	result.Version = UserConfiguration.Version

	handleConfigValues(&result)
	return result
}

func handleGemboxConversion(config *Config) {
	localGemboxData, _ := os.ReadFile(config.GemboxFilename)

	var localGembox gembox.GemBox
	if config.Version == "0.4.6c" { // 0.4.6c was the first version to use old misfin(C) gembox format
		localGembox = gembox.ParseGemBox_MisfinC_old(config.GemboxFilename, string(localGemboxData))
	} else if strings.HasSuffix(config.Version, "c") { // 0.4.7c was the first version to use new gembox format
		localGembox = gembox.ParseGemBox(config.GemboxFilename, string(localGemboxData))
	} else { // All non-"c" versions used old misfin(B) gembox format
		localGembox = gembox.ParseGemBox_MisfinB_old(config.GemboxFilename, string(localGemboxData))
	}

	writeErr := os.WriteFile(config.GemboxFilename, []byte(localGembox.String()), 0600)
	if writeErr != nil {
		panic(writeErr)
	}

	config.Version = Version
}

// NOTE: If the --gembox flag has been set, this will get that value as the gembox automatically and not touch the config gembox.
func fetchGembox(config Config) {
	if config.GeminiFetchAddress == "" {
		return
	}

	// TODO: Implement lockfile so that you cannot have two misfinmail instances running (as they may conflict when writing changes to gembox)

	// If no time configured, then set the last fetched gemmail datetime to the last message's receive date in the gembox, and save the config
	if (config.LastFetchedGemmailDatetime == time.Time{}) {
		localGemboxData, _ := os.ReadFile(config.GemboxFilename)
		localGembox := gembox.ParseGemBox("", string(localGemboxData))

		if len(localGembox.Mails) > 0 {
			config.LastFetchedGemmailDatetime = localGembox.Mails[len(localGembox.Mails)-1].Timestamps[0].Value
			saveConfig(config)
		}
	}

	// Fetch gembox from gemini and save it to gembox location
	client := gemini.DefaultClient
	resp, gemini_err := client.FetchWithCert(config.GeminiFetchAddress, config.CertFile, config.CertFile)
	if gemini_err != nil {
		panic(gemini_err)
	}
	gemboxData, readErr := io.ReadAll(resp.Body)
	if readErr != nil {
		panic(readErr)
	}

	gbox := gembox.ParseGemBox(config.GeminiFetchAddress, string(gemboxData))
	toDelete := make([]bool, len(gbox.Mails))
	for i, mail := range gbox.Mails {
		if config.LastFetchedGemmailDatetime.After(mail.Timestamps[0].Value) || config.LastFetchedGemmailDatetime == mail.Timestamps[0].Value {
			toDelete[i] = true
		}
	}

	offset := 0
	for i, v := range toDelete {
		if v {
			gbox.RemoveGemMail(i - offset)
			offset += 1
		}
	}

	if len(gbox.Mails) > 0 {
		config.LastFetchedGemmailDatetime = gbox.Mails[len(gbox.Mails)-1].Timestamps[0].Value
	}
	saveConfig(config)

	// Append new gembox data to existing local gembox, if there is any. If nothing to append, skip this.
	if len(gbox.Mails) > 0 {
		localGemboxData, _ := os.ReadFile(config.GemboxFilename)
		localGembox := gembox.ParseGemBox("", string(localGemboxData))

		for _, mail := range gbox.Mails {
			localGembox.AppendGemMail(mail)
		}

		writeErr := os.WriteFile(config.GemboxFilename, []byte(localGembox.String()), 0600)
		if writeErr != nil {
			panic(writeErr)
		}
	}

	// Save info of last message fetched.
}
